document.addEventListener('DOMContentLoaded', function() {

  if(document.getElementById('theform')) {
    var theform = document.getElementById('theform');
    var btn = document.getElementById('submitBtn');
    var cancel = document.getElementById('cancelBtn');
    var text = document.getElementById('productIN');

    btn.addEventListener('click', function(e) {
      e.preventDefault();
      btn.disabled = true;
      cancel.style.display = "block";
      btn.innerHTML = "Flying";

      var counter = 0;
      var looper = setInterval(function() { 
          btn.innerHTML = btn.innerHTML + ".";
          counter++;

          if (counter >= 3)
          {
              clearInterval(looper);
          }

      }, 1000);

      var products = text.value.split(',');
      for(var i = 0; i < products.length; i++) {
        products[i].split(' ').join('%20');
      }

      var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 

      xmlhttp.open("POST", "http://localhost:3000/api");

      xmlhttp.onreadystatechange = function() {
        console.log(xmlhttp.readyState)
        console.log(xmlhttp.DONE);
        if (xmlhttp.readyState === xmlhttp.DONE) {
            var jsonResponse = JSON.parse(xmlhttp.responseText);
            
            chrome.storage.local.set({cache: jsonResponse, cacheTime: Date.now()}, function() {
              var newURL = "./results.html";
              chrome.tabs.create({ url: newURL, active: false});
            });
            
        }
      }


      xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      xmlhttp.send(JSON.stringify({products: products}));

      return false;

    });
  }
  else {
    
    
  }



  /*
  var checkPageButton = document.getElementById('checkPage');
  checkPageButton.addEventListener('click', function() {

    chrome.tabs.getSelected(null, function(tab) {
      d = document;

      var f = d.createElement('form');
      f.action = 'http://gtmetrix.com/analyze.html?bm';
      f.method = 'post';
      var i = d.createElement('input');
      i.type = 'hidden';
      i.name = 'url';
      i.value = tab.url;
      f.appendChild(i);
      d.body.appendChild(f);
      f.submit();
    });
  }, false);
  */
}, false);