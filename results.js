document.addEventListener('DOMContentLoaded', function() {

  var table1 = document.getElementById('table1');
  var table2 = document.getElementById('table2');
  var table3 = document.getElementById('table3');

  var amazon;
  var ebay;

  chrome.storage.local.get(['cache', 'cacheTime'], function(items) {
      amazon = bubbleSortAmazon(items.cache.Amazon);
      ebay = bubbleSortEbay(items.cache.Ebay);

      for(var i = amazon.length-1; i >= 0; i--) {
        table1.innerHTML = table1.innerHTML + '<div class="row"> <div class="cell">' + amazon[i].title + '</div><div class="cell">' + amazon[i].price.FormattedPrice[0] + '</div><div class="cell"><a href="' + amazon[i].url + '">Amazon</a></div><div class="cell">' + amazon[i].price.CurrencyCode[0] + '</div></div>'
      }

      for(var i = ebay.length-1; i >= 0; i--) {
        table2.innerHTML = table2.innerHTML + '<div class="row"> <div class="cell">' + ebay[i].title + '</div><div class="cell">$' + ebay[i].price.amount + '</div><div class="cell">' + ((ebay[i].shippingType=='Free')?'Yes':'No') + '</div><div class="cell">' + ebay[i].location + '</div><div class="cell"><a href="' + ebay[i].url + '">Ebay</a></div><div class="cell">' + ebay[i].price.currencyId + '</div></div>'
      }


      var currentA = 0;
      var currentE = 0;
      var tempTop = 0;

      for(var i = 0; i < amazon.length; i++) {
        currentA = 0;
        currentE = 0;
        tempTop = 0;

        for(var j = 0; j < ebay.length; j++) {
            if(amazon[i] && ebay[j]) {
              console.log(getSimilarity(amazon[i].title, ebay[j].title));
                if(getSimilarity(amazon[i].title, ebay[j].title) > tempTop) {
                    currentA = i;
                    currentE = j;
                }
            }
        }
        

        table3.innerHTML = table3.innerHTML + '<div class="row"> <div class="cell">' + amazon[currentA].title + '</div><div class="cell">' + amazon[currentA].price.FormattedPrice[0] + '</div><div class="cell">Unknown</div><div class="cell">Unknown</div><div class="cell"><a href="' + amazon[currentA].url + '">Amazon</a></div><div class="cell">' + amazon[currentA].price.CurrencyCode[0] + '</div></div>'
        table3.innerHTML = table3.innerHTML + '<div class="row"> <div class="cell">' + ebay[currentE].title + '</div><div class="cell">$' + ebay[currentE].price.amount + '</div><div class="cell">' + ((ebay[currentE].shippingType=='Free')?'Yes':'No') + '</div><div class="cell">' + ebay[currentE].location + '</div><div class="cell"><a href="' + ebay[currentE].url + '">Ebay</a></div><div class="cell">' + ebay[currentE].price.currencyId + '</div></div>'
        table3.innerHTML = table3.innerHTML + '<div class="row blue"> <div class="cell-b"></div><div class="cell-b"></div><div class="cell-b"></div><div class="cell-b"></div><div class="cell-b"></div><div class="cell-b"></div>'
      
      }

  });


});

function bubbleSortAmazon(arr){
   var len = arr.length;
   for (var i = len-1; i>=0; i--){
     for(var j = 1; j<=i; j++){
       if(parseInt(arr[j-1].price.Amount[0])>parseInt(arr[j].price.Amount[0])){
          // console.log('price ' + arr[j-1].price.Amount[0] + ' from ' + arr[j-1].title + ' is greater than');
          // console.log('price ' + arr[j].price.Amount[0] + ' from ' + arr[j].title);
           var temp = arr[j-1];
           arr[j-1] = arr[j];
           arr[j] = temp;
        }
     }
   }
   return arr;
}

function bubbleSortEbay(arr){
   var len = arr.length;
   for (var i = len-1; i>=0; i--){
     for(var j = 1; j<=i; j++){
       if(parseInt(arr[j-1].price.amount)>parseInt(arr[j].price.amount)){
           var temp = arr[j-1];
           arr[j-1] = arr[j];
           arr[j] = temp;
        }
     }
   }
   return arr;
}


function similarity(s1, s2) {
  var longer = s1;
  var shorter = s2;
  if (s1.length < s2.length) {
    longer = s2;
    shorter = s1;
  }
  var longerLength = longer.length;
  if (longerLength == 0) {
    return 1.0;
  }
  return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
}

function getSimilarity ( str1, str2 ) {
  var count = 0;

  var words1 = str1.split(/\s+/g),
    words2 = str2.split(/\s+/g),
    i,
    j;

  for (i = 0; i < words1.length; i++) {
      for (j = 0; j < words2.length; j++) {
          if (words1[i].toLowerCase() == words2[j].toLowerCase()) {
             count += 1;
          }
      }
  }

  return count;
}


function editDistance(s1, s2) {
  s1 = s1.toLowerCase();
  s2 = s2.toLowerCase();

  var costs = new Array();
  for (var i = 0; i <= s1.length; i++) {
    var lastValue = i;
    for (var j = 0; j <= s2.length; j++) {
      if (i == 0)
        costs[j] = j;
      else {
        if (j > 0) {
          var newValue = costs[j - 1];
          if (s1.charAt(i - 1) != s2.charAt(j - 1))
            newValue = Math.min(Math.min(newValue, lastValue),
              costs[j]) + 1;
          costs[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0)
      costs[s2.length] = lastValue;
  }
  return costs[s2.length];
}








